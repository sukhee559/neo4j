from neo4j import GraphDatabase

#def add_friend(tx):
 #   tx.run("LOAD CSV WITH HEADERS FROM 'file:///product_attribute.csv' AS row MERGE (p:Product {name:row.product_domain}) MERGE (m:Main {name:row.product_main_type}) CREATE (p)-[a:prod_to_main]->(m) MERGE (s:Sub {name:row.product_sub_type}) CREATE (m)-[b:main_to_sub]->(s) MERGE(t:Attribute {name:row.product_attribute, tag:row.question_id}) CREATE (s)-[c:sub_to_attribute]->(t)")

def attributes(tx, name, limit):
    data = []
    for record in tx.run("MATCH (s)-[:sub_to_attribute]->(t) WHERE s.name = $name "
                          "RETURN t.name ORDER BY t.name LIMIT {}".format(limit), name=name):
        #print(record["t.name"])
        data.append(record["t.name"])
    return data

def product(tx, name, limit):
    data = []
    for record in tx.run("MATCH (p)-[:prod_to_main]->(m) WHERE p.name = $name "
                          "RETURN m.name ORDER BY m.name LIMIT {}".format(limit), name=name):
        #print(record["m.name"])
        data.append(record["m.name"])
    return data
def Subtype(tx, name, limit):
    data = []
    for record in tx.run("MATCH (Main)-[:main_to_sub]->(Sub) WHERE Main.name = $name "
                          "RETURN Sub.name ORDER BY Sub.name LIMIT {}".format(limit), name=name):
        #print(record["t.name"])
        data.append(record["Sub.name"])
    return data
def recommendation(text, types, limit):
    driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
    with driver.session() as session:
        if types == "attributes":
            returndata = session.read_transaction(attributes, text, limit)
        elif types == "product":
            returndata = session.read_transaction(product, text, limit)
        elif types == "Subtype":
            returndata = session.read_transaction(Subtype, text, limit)   
        else:
            returndata = session.read_transaction(product, text, limit) 
        #print(type(returndata))
    driver.close()
    return returndata

# text = 'алтан карт'
# limit = '5'
# types = 'Subtype'
# a = recommendation(text, limit, types)
# print(a)




# LOAD CSV WITH HEADERS FROM "file:///product_attribute.csv" AS row
# MERGE (p:Product {name: row.product_domain})  
# MERGE (m:Main {name: row.product_main_type}) MERGE (p)-[b:main]->(m)
# MERGE (s:Sub {name: row.product_sub_type}) MERGE (m)-[a:prod]->(s) 
# MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id}) MERGE (s)-[c:subtype]->(t)
# from neo4j import GraphDatabase

