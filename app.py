from flask import Flask, url_for, request
from logging.config import thread
import json
import logging

import requests
import query

app = Flask(__name__)

@app.route("/neo4j")
def hello():
    return "<h1 style='color:blue'>Hello World! Golomt NLP serv</h1>"

@app.route('/neo4j/search', methods=['POST', 'GET'])
def chatbot_service():
    try:
        sentence = []
        if request.method == 'GET':
            #sender = request.args['sender']
            sentence = request.args['message']
            types = request.args['types']
            limit = request.args['limit']
        elif request.method == 'POST':
            data = request.get_json()
            #sender = data['sender']
            sentence = data['message']
            types = request.args['types']
            limit = request.args['limit']
        answer = query.recommendation(sentence, types, limit)
        #answer = answer.join(',')
        answer = listToString(answer)
        #answer = answer.split()
        return answer


        #return json.dumps(answer)

    except Exception as e:
        logging.error(" ERROR IS HERE : " + str(e))
    
        return 'Failed'

# Function to convert   
def listToString(s):  
    
    # initialize an empty string 
    str1 = " , " 
    
    # return string   
    return (str1.join(s)) 

if __name__ == "__main__":
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    #app.logger.setLevel(gunicorn_logger.level)
    app.run(host='127.0.0.1', port=6012, debug=True)