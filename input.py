from neo4j.v1 import GraphDatabase

driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "letmein"))
session = driver.session()

#session.run("USING PERIODIC COMMIT LOAD CSV FROM 'file:///Desktop/movies-python-bolt-master1/product_attribute.csv' AS row MERGE (p:Product {name:row.product_domain}) MERGE (p)-[a:prod]->(m) MERGE (m:Main {name:row.product_main_type}) MERGE (m)-[b:main]->(s) MERGE (s:Sub {name:row.product_sub_type}) MERGE (s)-[c:subtype]->(t) MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id})")
#result = "USING PERIODIC COMMIT LOAD CSV FROM 'file:///Desktop/movies-python-bolt-master1/product_attribute.csv' AS row MERGE (p:Product {name:row.product_domain}) MERGE (p)-[a:prod]->(m) MERGE (m:Main {name:row.product_main_type}) MERGE (m)-[b:main]->(s) MERGE (s:Sub {name:row.product_sub_type}) MERGE (s)-[c:subtype]->(t) MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id})"
#session.close()

#cql = ''' USING PERIODIC COMMIT LOAD CSV FROM 'file:///Desktop/movies-python-bolt-master1/product_attribute.csv' AS row MERGE (p:Product {name:row.product_domain}) MERGE (p)-[a:prod]->(m) MERGE (m:Main {name:row.product_main_type}) MERGE (m)-[b:main]->(s) MERGE (s:Sub {name:row.product_sub_type}) MERGE (s)-[c:subtype]->(t) MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id}) '''
#driver.run(" LOAD CSV WITH HEADERS FROM 'file:///product_attribute.csv' AS row MERGE (p:Product {name: row.product_domain}) MERGE (m:Main {name: row.product_main_type}) MERGE (p)-[a:prod_to_main]->(m) MERGE (s:Sub {name: row.product_sub_type})  MERGE (m)-[b:main_to_sub]->(s) MERGE (t:Attribute {name:row.product_attribute}) MERGE (s)-[c:sub_to_attribute]->(t)")

session.run("LOAD CSV WITH HEADERS FROM 'file:///product_attribute.csv' AS row MERGE (p:Product {name: row.product_domain}) MERGE (m:Main {name: row.product_main_type}) MERGE (p)-[a:prod_to_main]->(m) MERGE (s:Sub {name: row.product_sub_type}) MERGE (m)-[b:main_to_sub]->(s) MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id}) MERGE (s)-[c:sub_to_attribute]->(t)")


session.close()

# LOAD CSV FROM "file:///product_attribute.csv" AS row 
# MERGE (p:Product {name: row.product_domain}) 
# MERGE (m:Main {name: row.product_main_type}) CREATE (p)-[a:product_to_main]->(m) 
# CREATE (m)-[b:main_to_sub]->(s) 
# MERGE (s:Sub {name: row.product_sub_type}) CREATE (s)-[c:sub_to_attribute]->(t) 
# MERGE (t:Attribute {name:row.product_attribute, tag:row.question_id})

#########data input###########
# CREATE CONSTRAINT ON (p:Product) ASSERT p.name IS UNIQUE;
# CREATE CONSTRAINT ON (m:Main) ASSERT m.name IS UNIQUE;
# CREATE CONSTRAINT ON (s:Sub) ASSERT s.name IS UNIQUE;
# CREATE CONSTRAINT ON (t:Attribute) ASSERT t.name IS UNIQUE;

# LOAD CSV WITH HEADERS FROM "file:///product_attribute.csv" AS row MERGE (p:Product {name: row.product_domain})
# MERGE (m:Main {name: row.product_main_type})
# MERGE (p)-[a:prod_to_main]->(m)
# MERGE (s:Sub {name: row.product_sub_type}) 
# MERGE (m)-[b:main_to_sub]->(s)
# MERGE (t:Attribute {name:row.product_attribute})
# MERGE (s)-[c:sub_to_attribute]->(t)

# LOAD CSV WITH HEADERS FROM "file:///product_attribute.csv" AS row MERGE (p:Product {name: row.product_domain})
# MERGE (m:Main {name: row.product_main_type})
# MERGE (p)-[a:prod_to_main]->(m)
# MERGE (s:Sub {name: row.product_sub_type}) 
# MERGE (m)-[b:main_to_sub]->(s)
# MERGE (t:Attribute {name:row.product_attribute})
# MERGE (s)-[c:sub_to_attribute]->(t)